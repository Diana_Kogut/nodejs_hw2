const express = require('express');
const router = express.Router();

const { createNote, updateNote, deleteNote, checkNote, showNotes, findNoteById } = require('../controllers/noteController');

const authMiddleware = require('../middlewares/authMiddleware');

router.get('/notes', authMiddleware, showNotes);
router.get('/notes/:id', authMiddleware, findNoteById);
router.post('/notes', authMiddleware, createNote);
router.put('/notes/:id', authMiddleware, updateNote);
router.delete('/notes/:id', authMiddleware, deleteNote);
router.patch('/notes/:id', authMiddleware, checkNote);


module.exports = router;
const express = require('express');
const router = express.Router();

const { showProfileInfo, deleteProfile, changePassword } = require('../controllers/userController');

const authMiddleware = require('../middlewares/authMiddleware');


router.get('/users/me', authMiddleware, showProfileInfo);
router.delete('/users/me', authMiddleware, deleteProfile);
router.patch('/users/me', authMiddleware, changePassword);



module.exports = router;
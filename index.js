const express = require('express');
const mongoose = require('mongoose');

const { port } = require('./config/server');
const { dbUrl } = require('./config/database');

const app = express();

const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');

mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
});

app.use(express.json());

app.use('/api', userRouter);
app.use('/api', authRouter);
app.use('/api', noteRouter);

app.listen(port, () => console.log(`Server started on ${port} port`))
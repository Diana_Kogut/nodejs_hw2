const mongoose = require('mongoose');

module.exports = mongoose.model('error', {
    message: {
        type: String
    }
})
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('user', new Schema({
    username: {
        required: true,
        unique: true,
        type: String
    },
    createdDate: {
        required: true,
        type: String
    },
    password: {
        required: true,
        type: String
    }
}));
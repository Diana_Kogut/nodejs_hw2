const mongoose = require('mongoose');

module.exports = mongoose.model('note', {
    username: {
        required: true,
        type: String
    },
    password: {
        required: true,
        type: String
    }
})
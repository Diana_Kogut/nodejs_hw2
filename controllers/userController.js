const User = require('../models/user');

module.exports.showProfileInfo = (req, res) => {
    const userId = req.user._id;

    User.findById(userId).exec()
        .then((user) => {
            if (!user) res.status(400).json({ message: 'User not found' })
            res.json({
                _id: userId,
                createdDate: user.createdDate,
                username: user.username
            });
        })
        .catch(err => {
            res.status(500).json({ message: 'User not found' });
        });
};

module.exports.deleteProfile = (req, res) => {
    const userId = req.user._id;
    if (!userId) res.status(400).json({ message: 'User not found' });


    User.findByIdAndDelete(userId).exec()
        .then(() => {
            res.json({ message: 'Your profile has been successfully deleted' });
        })
        .catch(err => {
            res.status(500).json({ message: err.message });
        });
};

module.exports.changePassword = (req, res) => {
    const username = req.user.username;
    User.updateOne({
            username
        }, { password: req.body.newPassword }).exec()
        .then((user) => {
            if (req.body.password !== user.password) {
                res.status(400).json({ message: "You entered an incorrect password" });
            }
            res.json({ status: 'You changed your password' });
        })
        .catch(err => {
            res.status(500).json({ message: err.message });
        });
};
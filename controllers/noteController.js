const Note = require('../models/note');

module.exports.createNote = (req, res) => {
    const { completed, text } = req.body;

    if (!completed || !text) res.status(400).json({ message: 'Enter correct data' });

    const note = new Note({ userId: req.user._id, completed, text, createdDate: new Date() });
    note.save()
        .then(() => {
            res.json({ message: 'success' });
        })
        .catch(err => {
            res.status(500).json({ message: err.message });

        });
};

module.exports.updateNote = (req, res) => {
    const noteId = req.params.id;

    if (!noteId) res.status(400).json({ message: 'No notes found' });

    Note.findOneAndUpdate({
            _id: noteId,
            userId
        }, {
            $set: req.body
        }).exec()
        .then(() => {
            res.json({ message: 'success' });
        })
        .catch(err => {
            res.status(500).json({ message: err.message });
        });
};

module.exports.deleteNote = (req, res) => {
    const noteId = req.params.id;
    const userId = req.user._id;

    if (!noteId) res.status(400).json({ message: 'No notes found' });

    Note.findOneAndDelete({
            _id: noteId,
            userId: userId
        }).exec()
        .then(() => {
            res.json({
                message: 'success'
            });
        })
        .catch(err => {
            res.status(500).json({ message: err.message });
        });
};

module.exports.showNotes = (req, res) => {
    const userId = req.user._id;

    Note.find({
            userId
        }).exec()
        .then((notes) => {

            if (!notes) res.status(400).json({ message: 'No notes found' });

            res.json({
                notes
            });
        })
        .catch(err => {
            res.status(500).json({ message: err.message });
        });
};

module.exports.findNoteById = (req, res) => {
    const noteId = req.params.id;

    if (!noteId) res.status(400).json({ message: 'No notes found' });

    Note.findOne({
            _id: noteId
        }).exec()
        .then((note) => {
            res.json({
                note
            });
        })
        .catch(err => {
            res.status(500).json({ message: err.message });
        });
};

module.exports.checkNote = (req, res) => {
    const noteId = req.params.id;
    const userId = req.user._id;
    let completedStatus;

    if (!noteId) res.status(400).json({ message: 'No notes found' });

    Note.findOne({
            _id: noteId
        }).exec()
        .then((data) => {
            completedStatus = data.completed;
            return Note.findOneAndUpdate({
                _id: noteId,
                userId
            }, {
                $set: {
                    completed: completedStatus === true ? false : true
                }
            }).exec()
        })
        .then(() => {
            res.json({ message: 'Success' });
        })
        .catch(err => {
            res.status(500).json({ message: err.message });
        });
};
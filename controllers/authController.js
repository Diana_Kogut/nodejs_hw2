const jwt = require('jsonwebtoken');
const User = require('../models/user');

const { secret } = require('../config/auth');
const user = require('../models/user');

module.exports.register = (req, res) => {
    const { username, password } = req.body;

    let createdDate = new Date();

    const user = new User({ username, password, createdDate });

    user.save()
        .then(() => {
            res.json({ message: 'success' });
        })
        .catch(err => {
            res.status(500).json({ message: err.message });
        });
};

module.exports.login = (req, res) => {
    const { username, password } = req.body;

    User.findOne({ username, password }).exec()
        .then(user => {
            if (!user) {
                return res.status(400).json({ message: 'No user with such username' });
            }
            res.json({ message: 'success', token: jwt.sign(JSON.stringify(user), secret) });
        })
        .catch(err => {
            res.status(500).json({ message: err.message });
        });
};